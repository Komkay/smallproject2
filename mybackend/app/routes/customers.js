const express = require('express')
const customerController = require('../controller/customerController')
const router = express.Router()
/* GET users listing. */

router.get('/', customerController.getCustomers)
router.get('/:id', customerController.getCustomer)
router.post('/', customerController.addCustomer)
router.put('/', customerController.updateCustomer)
router.delete('/:id', customerController.deleteCustomer)

module.exports = router
