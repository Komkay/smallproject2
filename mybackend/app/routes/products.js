const express = require('express')
const productController = require('../controller/productController')
const router = express.Router()
/* GET users listing. */

router.get('/', productController.getProducts)
router.get('/:id', productController.getProduct)

router.post('/', productController.addProduct)
router.put('/', productController.updateProduct)
router.delete('/:id', productController.deleteProduct)

module.exports = router
