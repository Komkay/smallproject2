const express = require('express')
const userController = require('../controller/UserController')
const router = express.Router()

router.get('/', userController.getUsers)
router.get('/:id', userController.getUser)
router.get('/:login', userController.getUser)
router.post('/', userController.addUser)
router.put('/', userController.updateUser)
router.delete('/:id', userController.deleteUser)

module.exports = router
