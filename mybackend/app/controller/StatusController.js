const Status = require('./statusconnect.js')
const statusController = {
  async updateProduct (req, res, next) {
    const payload = req.body
    // res.json(usersController.updateUser(payload))
    try {
      const status = await Status.updateOne({ _id: payload._id }, payload)
      res.json(status)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getStatus (req, res, next) {
    try {
      const status = await Status.find({})
      res.json(status)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addStatus (req, res, next) {
    const payload = req.body
    const status = new Status(payload)
    try {
      await status.save()
      res.json(status)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = statusController
