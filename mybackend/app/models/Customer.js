const mongoose = require('mongoose')
const Schema = mongoose.Schema

mongoose.connect('mongodb://localhost/small_pro', { useNewUrlParser: true })
const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect2')
})

const customerSchema = new Schema({
  name: String,
  address: String,
  dob: String,
  gender: String,
  mobile: String,
  email: String
})

const Customer = mongoose.model('Customer', customerSchema)

Customer.find(function (err, Customers) {
  if (err) return console.log(err)
  console.log(Customers)
})
module.exports = Customer
