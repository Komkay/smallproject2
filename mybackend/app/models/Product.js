const mongoose = require('mongoose')
const Schema = mongoose.Schema

mongoose.connect('mongodb://localhost/small_pro', { useNewUrlParser: true })
const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect1')
})

const productSchema = new Schema({
  name: String,
  price: String,
  cost: String
})

const Product = mongoose.model('Product', productSchema)

Product.find(function (err, Products) {
  if (err) return console.log(err)
})
module.exports = Product
